using UnityEngine;

namespace Hawki
{
    public interface IGlobalTransfrom 
    {
        Transform transform { get; }
    }
} 

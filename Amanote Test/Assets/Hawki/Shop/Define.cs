namespace Hawki.Shop
{
    public partial class ShopId
    {
        public const string NO_ADS = "NO_ADS";
    }

    public partial class PriceId
    {
        public const string ADS = "ADS";
    }

    public partial class ShopPosition
    {
        public const string Shop = "shop";
    }
}

namespace Hawki.ResourcesLoader
{
    public partial class ResourcesLoaderLink
    {
        public const string UI_SHOPITEM = "Prefabs/UI/ShopItem";
        public const string UI_SHOPGROUPITEM = "Prefabs/UI/ShopGroupItem";
        public const string UI_SHOPITEMLISTITEMVIEWBYTYPE = "Prefabs/UI/ShopItemListItemViewByType";
    }
}
# Hawki Library

- Author: Nguyen Minh Hao
- Gmail: haominh460@gmail.com

The Hawki Library is a set of tools developed by Nguyen Minh Hao to aid in building applications and games using the Unity game engine. This library provides useful utilities, helper classes, and functions to streamline development and enhance flexibility of projects. Utilize the available classes and functions within the library to accelerate the development process.
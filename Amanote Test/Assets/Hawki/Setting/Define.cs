namespace Hawki.Setting
{
    public static class URL
    {
        public const string DEFAULT_TERM_OF_USE = "https://www.antada.com.vn/terms-and-conditions";
        public const string DEFAULT_PRIVACY_POLICY = "https://www.antada.com.vn/privacy-policy";
    }
}

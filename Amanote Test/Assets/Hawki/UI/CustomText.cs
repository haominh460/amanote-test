using TMPro;

namespace Hawki.UI
{
    public class CustomText : CustomComponent<TMP_Text>
    {
        protected TMP_Text _text => _component;
    }
}

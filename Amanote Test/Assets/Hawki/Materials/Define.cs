namespace Hawki.GameFlow
{
    public partial class ResourcesLink
    {
        public const string MATERIALS = "Materials";
    }
}

namespace Hawki.Materials
{
    public partial class MaterialId
    {
        public const string GRAY_SCALE = "GrayScale";
    }
}
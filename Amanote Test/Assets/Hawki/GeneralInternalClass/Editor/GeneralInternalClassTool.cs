﻿using Hawki.Config;
using Hawki.SaveData;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Hawki.GeneralInternalClass.Editor
{
    public class GeneralInternalClassTool
    {
        [MenuItem("Hawki/General Internal Class/Generate All")]
        public static void GeneralAllClassManager()
        {
            GeneralInternalClass<SaveDataBase>(className: nameof(SaveDataManager));

            GeneralInternalClass<ConfigDataBase>(className: nameof(ConfigManager));
        }

        public static void GenerateClass(List<string> libraryNames, string namespaceName, string managerName, List<string> classNames)
        {
            string internalClassName = "General" + managerName;
            string code = GenerateCode(libraryNames, namespaceName, managerName, internalClassName, classNames);
            string path = "Assets/Hawki_General/Scripts";

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string filePath = Path.Combine(path, $"{internalClassName}.cs");

            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                Debug.LogWarning($"Existing {internalClassName}.cs has been deleted in {path}.");
            }

            File.WriteAllText(filePath, code);
            Debug.Log($"File {internalClassName}.cs has been generated in {path}!");

            AssetDatabase.Refresh();
        }

        private static void GeneralInternalClass<T>(string className)
        {
            var allType = TypeUtilities.FindAllDerivedTypesInDomain<T>();

            var nameSpace = typeof(T).Namespace;

            var allUsingNamespace = allType.Select(x => x.Namespace).Distinct().ToList();

            GenerateClass(allUsingNamespace, nameSpace, className, allType.Select(x => x.Name).ToList());
        }

        private static string GenerateCode(List<string> libraryName, string nameSpaceName, string managerName, string internalClassName, List<string> classNames)
        {
            StringBuilder builder = new StringBuilder();

            builder.AppendLine("// This file is automatically generated. Do not modify!");
            builder.AppendLine("// Only operates when the compiler has no errors.");
            builder.AppendLine();

            foreach (var libra in libraryName)
            {
                builder.AppendLine($"using {libra};");
            }

            builder.AppendLine();
            builder.AppendLine($"namespace {nameSpaceName}.General");
            builder.AppendLine("{");
            builder.AppendLine($"    public class {internalClassName} : RuntimeSingleton<{internalClassName}>");
            builder.AppendLine("    {");

            foreach (var className in classNames)
            {
                builder.AppendLine($"        public {className} {className} => {managerName}.Instance.GetData<{className}>();");
            }

            builder.AppendLine("    }");
            builder.AppendLine("}");

            return builder.ToString();
        }
    }
}

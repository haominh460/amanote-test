using AmanotesTest.Core;
using DG.Tweening;
using Hawki.EventObserver;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace AmanotesTest.UI
{
    public class ColumnView : MonoBehaviour, IPointerDownHandler, IRegister
    {
        public int id;

        [SerializeField] private Image _effect;

        private void OnEnable()
        {
            EventObs.Instance.AddRegister(EventName.GP_BEGIN, this);
            EventObs.Instance.AddRegister(EventName.GP_COLUMN_DOWN, this);
        }

        private void OnDisable()
        {
            EventObs.Instance.RemoveRegister(EventName.GP_BEGIN, this);
            EventObs.Instance.RemoveRegister(EventName.GP_COLUMN_DOWN, this);
        }
        public void OnPointerDown(PointerEventData eventData)
        {
            GamePlayService.Instance.ColumnDown(id);
        }

        public void OnEvent(string eventId, EventBase data)
        {
            switch (eventId)
            {
                case EventName.GP_BEGIN:
                    _effect.gameObject.SetActive(false);
                    break;
                case EventName.GP_COLUMN_DOWN:
                    var d1 = data as GPColumnClickEvent;

                    if (d1.columnId == id)
                    {
                        _effect.gameObject.SetActive(true);
                        _effect.DOKill();
                        _effect.DOFade(0.8f, 0.3f)
                            .From(0)
                            .SetLoops(-1, LoopType.Yoyo);
                    }
                    break;
            }
        }
    }

}
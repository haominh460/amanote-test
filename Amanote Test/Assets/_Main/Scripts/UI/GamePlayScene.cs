using AmanotesTest.Core;
using AmanotesTest.Core.UI;
using DG.Tweening;
using Hawki.EventObserver;
using Hawki.Scene;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace AmanotesTest.UI
{
    public class GamePlayScene : Controller, IMapViewManager, IRegister
    {
        [SerializeField] private Transform _topTrans;
        [SerializeField] private Transform _botTrans;
        [SerializeField] private Transform _nodeRoot;

        [SerializeField] private TMP_Text _pointText;

        private List<TMP_Text> _pointTextPool = new List<TMP_Text>();
        private Dictionary<string, NodeViewBase> _dictNodes = new Dictionary<string, NodeViewBase>();

        protected override void OnAwake()
        {
            base.OnAwake();

            _pointText.gameObject.SetActive(false);
        }

        public override string SceneName()
        {
            return SceneId.GAMEPLAY;
        }

        public Vector2 GetNodePosition(int columnId, float percent)
        {
            var x = 135 + 270 * columnId;
            var y = (_botTrans.localPosition.y - _topTrans.transform.localPosition.y) * percent + _topTrans.transform.localPosition.y;

            return new Vector2(x, y);
        }

        public float GetNodeLength(float percentLength)
        {
            return (_topTrans.localPosition.y - _botTrans.localPosition.y) * percentLength;
        }

        protected override void OnActive()
        {
            base.OnActive();

            EventObs.Instance.AddRegister(EventName.GP_NODE_ENABLE, this);
            EventObs.Instance.AddRegister(EventName.GP_NODE_DISABLE, this);
            EventObs.Instance.AddRegister(EventName.GP_BEGIN, this);

            GamePlayService.Instance.InitBattle(string.Empty);
        }

        protected override void OnHidden()
        {
            base.OnHidden();

            EventObs.Instance.RemoveRegister(EventName.GP_NODE_ENABLE, this);
            EventObs.Instance.RemoveRegister(EventName.GP_NODE_DISABLE, this);
            EventObs.Instance.RemoveRegister(EventName.GP_BEGIN, this);
        }

        private void ResetAll()
        {
            foreach (var node in _dictNodes.Values)
            {
                NodeViewResourcesLoader.Instance.FreeResources(node);
            }

            _dictNodes.Clear();
        }

        public void OnEvent(string eventId, EventBase data)
        {
            switch (eventId)
            {
                case EventName.GP_BEGIN:
                    ResetAll();
                    break;
                case EventName.GP_NODE_ENABLE:
                    var d1 = data as GPNodeEnableEvent;
                    foreach (var nodeId in d1.nodeIds)
                    {
                        var nodeViewModel = new NodeViewModel(nodeId)
                        {
                            nodeViewManager = this,
                            beginPercent = d1.dictBeginPositionPercent[nodeId],
                            columnId = d1.dictColumn[nodeId],
                            percentLength = d1.dictPercentLength[nodeId],
                            clicked = d1.dictClicked[nodeId],
                        };

                        var nodeView = NodeViewResourcesLoader.Instance.LoadResources(d1.dictType[nodeId], nodeViewModel, _nodeRoot);

                        _dictNodes.Add(nodeId, nodeView);
                    }
                    break;
                case EventName.GP_NODE_DISABLE:
                    var d2 = data as GPNodeDisableEvent;
                    foreach (var nodeId in d2.nodeIds)
                    {
                        if (_dictNodes.TryGetValue(nodeId, out var nodeView))
                        {
                            NodeViewResourcesLoader.Instance.FreeResources(nodeView);
                            _dictNodes.Remove(nodeId);
                        }
                    }
                    break;
            }
        }

        public float GetDeltaPositionPercent(float deltaY)
        {
            return deltaY / (_topTrans.transform.localPosition.y - _botTrans.transform.localPosition.y);
        }

        public void ShowPlusPoint(Vector2 localPosition, int point)
        {
            var newPoint = GetPointText();

            newPoint.transform.localPosition = localPosition;
            newPoint.text = $"+{point}";

            newPoint.DOFade(1, 0.1f).From(0);
            newPoint.transform.DOLocalMove(localPosition + Vector2.up * 300, 0.4f)
                .OnComplete(() =>
                {
                    newPoint.DOFade(0, 0.3f).OnComplete(() =>
                    {
                        newPoint.gameObject.SetActive(false);
                    });
                });
        }

        private TMP_Text GetPointText()
        {
            var target = _pointTextPool.Find(x => x.gameObject.activeSelf == false);
            if (target == null)
            {
                target = GameObject.Instantiate(_pointText, _pointText.transform.parent);
                _pointTextPool.Add(target);
            }

            target.gameObject.SetActive(true);

            return target;
        }
    }
    
}

using AmanotesTest.Core;
using DG.Tweening;
using Hawki.EventObserver;
using TMPro;
using UnityEngine;

namespace AmanotesTest
{
    public class PointText : MonoBehaviour, IRegister
    {
        [SerializeField] private TMP_Text _poinText;

        private void OnEnable()
        {
            EventObs.Instance.AddRegister(EventName.GP_BEGIN, this);
            EventObs.Instance.AddRegister(EventName.GP_POINT_ADD, this);
        }

        private void OnDisable()
        {
            EventObs.Instance.RemoveRegister(EventName.GP_BEGIN, this);
            EventObs.Instance.RemoveRegister(EventName.GP_POINT_ADD, this);
        }

        public void OnEvent(string eventId, EventBase data)
        {
            switch (eventId)
            {
                case EventName.GP_BEGIN:
                    _poinText.text = "0";
                    break;
                case EventName.GP_POINT_ADD:
                    var d1 = data as GPPointAddEvent;
                    _poinText.transform.DOKill();
                    _poinText.transform.DOScale(1.3f, 0.2f).SetLoops(2, LoopType.Yoyo).From(1);
                    _poinText.text = d1.totalPoint.ToString();
                    break;
            }
        }
    }
}

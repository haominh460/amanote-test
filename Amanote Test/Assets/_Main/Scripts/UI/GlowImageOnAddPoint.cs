using DG.Tweening;
using Hawki.EventObserver;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace AmanotesTest.UI
{
    public class GlowImageOnAddPoint : MonoBehaviour, IRegister
    {
        [SerializeField] private Image _targetImage;
        [SerializeField] private float _baseAlpha;

        private void OnEnable()
        {
            EventObs.Instance.AddRegister(EventName.GP_NODE_CLICK, this);
            EventObs.Instance.AddRegister(EventName.GP_NODE_HOLD_BEGIN, this);

            _targetImage.DOKill();
            _targetImage.DOFade(_baseAlpha, 0);
        }

        private void OnDisable()
        {
            EventObs.Instance.RemoveRegister(EventName.GP_NODE_CLICK, this);
            EventObs.Instance.RemoveRegister(EventName.GP_NODE_HOLD_BEGIN, this);
        }

        public void OnEvent(string eventId, EventBase data)
        {
            switch (eventId)
            {
                case EventName.GP_NODE_CLICK:
                case EventName.GP_NODE_HOLD_BEGIN:
                    StopAllCoroutines();
                    StartCoroutine(CRAnimation());
                    break;
            }
        }

        IEnumerator CRAnimation()
        {
            _targetImage.DOKill();
            _targetImage.DOFade(1f, 0.2f);

            yield return new WaitForSeconds(0.2f);

            _targetImage.DOKill();
            _targetImage.DOFade(_baseAlpha, 0.5f);
        }
    }
}

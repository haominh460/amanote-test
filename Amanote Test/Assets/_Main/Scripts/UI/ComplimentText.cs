using AmanotesTest.Core;
using DG.Tweening;
using Hawki.EventObserver;
using System.Collections;
using TMPro;
using UnityEngine;

namespace AmanotesTest
{
    public class ComplimentText : MonoBehaviour, IRegister
    {
        [SerializeField] private CanvasGroup _root;
        [SerializeField] private TMP_Text[] _compliments;
        [SerializeField] private TMP_Text _comboText;

        private void OnEnable()
        {
            EventObs.Instance.AddRegister(EventName.GP_BEGIN, this);
            EventObs.Instance.AddRegister(EventName.GP_POINT_ADD, this);
        }

        private void OnDisable()
        {
            EventObs.Instance.RemoveRegister(EventName.GP_BEGIN, this);
            EventObs.Instance.RemoveRegister(EventName.GP_POINT_ADD, this);
        }

        public void OnEvent(string eventId, EventBase data)
        {
            switch (eventId)
            {
                case EventName.GP_BEGIN:
                    _root.gameObject.SetActive(false);
                    _comboText.gameObject.SetActive(false);
                    break;
                case EventName.GP_POINT_ADD:
                    var d1 = data as GPPointAddEvent;

                    if (d1.compliment == Compliment.None)
                    {
                        return;
                    }

                    StopAllCoroutines();

                    _root.DOKill();
                    _root.DOFade(1, 0.2f);

                    _root.gameObject.SetActive(true);
                    foreach (var com in _compliments)
                    {
                        if (com.name == d1.compliment.ToString())
                        {
                            com.transform.DOKill();
                            com.transform.DOScale(1f, 0.2f)
                                .From(0)
                                .SetEase(Ease.OutBack);
                            com.gameObject.SetActive(true);
                        } else
                        {
                            com.gameObject.SetActive(false);
                        }
                    }

                    _comboText.gameObject.SetActive(d1.perfectCombo > 2);
                    _comboText.text = $"x{d1.perfectCombo}";
                    _comboText.transform.DOKill();
                    _comboText.transform.DOScale(1.3f, 0.2f).SetLoops(2, LoopType.Yoyo).From(1);

                    StartCoroutine(CRHide());
                    break;
            }
        }

        private IEnumerator CRHide()
        {
            yield return new WaitForSeconds(0.5f);

            _root.DOFade(0, 0.3f);
        }
    }
}

namespace Hawki.Scene
{
    public partial class SceneId
    {
        public const string GAMEPLAY = "GamePlayScene";
    }
}

namespace AmanotesTest
{
    public class Value
    {
        public static float MAP_LENGTH = 1800;

        public static float VISIBLE_POSITION_OFFSET = 0;
    }
}
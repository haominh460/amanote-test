using DG.Tweening;
using Hawki.EventObserver;
using Hawki.ResourcesLoader;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace AmanotesTest.Core.UI
{
    public class SimpleNodeView : NodeViewBase, IRegister, IPointerDownHandler
    {
        private NodeViewModel _model;

        [SerializeField] private RectTransform _mainRect;
        [SerializeField] private Image _clickEffect;
        [SerializeField] private Image _missingEffect;

        protected override void OnInit(Model model)
        {
            base.OnInit(model);

            _model = model as NodeViewModel;

            var newPosition = _model.nodeViewManager.GetNodePosition(_model.columnId, _model.beginPercent);
            transform.localPosition = newPosition;

            _mainRect.sizeDelta = new Vector2(_mainRect.sizeDelta.x, _model.nodeViewManager.GetNodeLength(_model.percentLength));

            EventObs.Instance.AddRegister(EventName.GP_NODE_UPDATE, this);
            EventObs.Instance.AddRegister(EventName.GP_NODE_CLICK, this);
            EventObs.Instance.AddRegister(EventName.GP_NODE_MISSING, this);

            _clickEffect.gameObject.SetActive(_model.clicked);
            _missingEffect.gameObject.SetActive(false);

            if (_model.clicked)
            {
                _clickEffect.DOFade(1, 0);
            }

        }

        public void OnClicked()
        {
            _model.clicked = true;
        }

        protected override void OnFree()
        {
            base.OnFree();

            EventObs.Instance.RemoveRegister(EventName.GP_NODE_UPDATE, this);
            EventObs.Instance.RemoveRegister(EventName.GP_NODE_CLICK, this);
            EventObs.Instance.RemoveRegister(EventName.GP_NODE_MISSING, this);

            _clickEffect.gameObject.SetActive(false);
            _missingEffect.gameObject.SetActive(false);
            _clickEffect.DOKill();
            _missingEffect.DOKill();
        }

        public void OnEvent(string eventId, EventBase data)
        {
            switch (eventId)
            {
                case EventName.GP_NODE_UPDATE:
                    var data1 = data as GPNodeUpdateEvent;

                    if (data1._dictPercentPosition.TryGetValue(_model.Id, out var percent))
                    {
                        var newPosition = _model.nodeViewManager.GetNodePosition(_model.columnId, percent);

                        transform.localPosition = newPosition;
                    }
                    break;
                case EventName.GP_NODE_CLICK:
                    var data2 = data as GPNodeClickEvent;
                    if (data2.node == _model.Id)
                    {
                        _clickEffect.gameObject.SetActive(true);
                        _clickEffect.DOFade(1, 0.2f)
                            .From(0);

                        _model.nodeViewManager.ShowPlusPoint(transform.localPosition, data2.point);
                    }
                    break;
                case EventName.GP_NODE_MISSING:
                    var data3 = data as GPNodeMissingEvent;
                    if (data3.nodeIds.Contains(_model.Id))
                    {
                        _missingEffect.gameObject.SetActive(true);
                        _missingEffect.DOFade(1, 0.3f)
                            .From(0)
                            .SetLoops(-1, LoopType.Yoyo);
                    }
                    break;
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            GamePlayService.Instance.ClickNode(_model.Id);
        }
    }
}

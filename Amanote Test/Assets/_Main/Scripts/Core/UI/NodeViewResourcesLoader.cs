using Hawki.ResourcesLoader;

namespace AmanotesTest.Core.UI
{
    public class NodeViewResourcesLoader : ResourcesLoader<NodeViewResourcesLoader, NodeViewBase>
    {
        protected override string ResourcesPath()
        {
            return "Prefabs/Nodes";
        }
    }
}

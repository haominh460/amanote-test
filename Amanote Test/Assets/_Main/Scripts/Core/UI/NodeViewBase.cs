using Hawki.ResourcesLoader;
using UnityEngine;

namespace AmanotesTest.Core.UI
{
    public interface INodeViewPositionHandler
    {
        public Vector2 GetNodePosition(int columnId, float percent);
        public float GetNodeLength(float percentLength);
        public float GetDeltaPositionPercent(float deltaY);
        public void ShowPlusPoint(Vector2 localPosition, int point);
    }

    public class NodeViewModel : Model
    {
        public INodeViewPositionHandler nodeViewManager;
        public int columnId;
        public float beginPercent;
        public bool clicked;
        public float holdPercent;
        public float percentLength;
        public NodeViewModel(string id) : base(id)
        {

        }
    }
    public class NodeViewBase : ResourcesPool
    {

    }
}

using DG.Tweening;
using Hawki.EventObserver;
using Hawki.ResourcesLoader;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace AmanotesTest.Core.UI
{
    public class HoldNodeView : NodeViewBase, IRegister, IPointerDownHandler, IPointerUpHandler
    {
        private NodeViewModel _model;

        [SerializeField] private RectTransform _mainRect;
        [SerializeField] private Slider _holdEffect;
        [SerializeField] private Image _missingEffect;

        protected override void OnInit(Model model)
        {
            base.OnInit(model);

            _model = model as NodeViewModel;

            var newPosition = _model.nodeViewManager.GetNodePosition(_model.columnId, _model.beginPercent);
            transform.localPosition = newPosition;

            _mainRect.sizeDelta = new Vector2(_mainRect.sizeDelta.x, _model.nodeViewManager.GetNodeLength(_model.percentLength));

            EventObs.Instance.AddRegister(EventName.GP_NODE_UPDATE, this);
            EventObs.Instance.AddRegister(EventName.GP_NODE_HOLD_BEGIN, this);
            EventObs.Instance.AddRegister(EventName.GP_NODE_HOLD_UPDATE, this);
            EventObs.Instance.AddRegister(EventName.GP_NODE_HOLD_END, this);
            EventObs.Instance.AddRegister(EventName.GP_NODE_MISSING, this);

            _holdEffect.gameObject.SetActive(_model.holdPercent > 0);
            _missingEffect.gameObject.SetActive(false);

            if (_model.holdPercent > 0)
            {
                _holdEffect.value = _model.holdPercent;
            }

        }

        public void OnClicked()
        {
            _model.clicked = true;
        }

        protected override void OnFree()
        {
            base.OnFree();

            EventObs.Instance.RemoveRegister(EventName.GP_NODE_UPDATE, this);
            EventObs.Instance.RemoveRegister(EventName.GP_NODE_HOLD_BEGIN, this);
            EventObs.Instance.RemoveRegister(EventName.GP_NODE_HOLD_UPDATE, this);
            EventObs.Instance.RemoveRegister(EventName.GP_NODE_HOLD_END, this);
            EventObs.Instance.RemoveRegister(EventName.GP_NODE_MISSING, this);

            _holdEffect.gameObject.SetActive(false);
            _missingEffect.gameObject.SetActive(false);
            _holdEffect.DOKill();
            _missingEffect.DOKill();
        }

        public void OnEvent(string eventId, EventBase data)
        {
            switch (eventId)
            {
                case EventName.GP_NODE_UPDATE:
                    var data1 = data as GPNodeUpdateEvent;

                    if (data1._dictPercentPosition.TryGetValue(_model.Id, out var percent))
                    {
                        var newPosition = _model.nodeViewManager.GetNodePosition(_model.columnId, percent);

                        transform.localPosition = newPosition;
                    }
                    break;
                case EventName.GP_NODE_HOLD_BEGIN:
                    var d2 = data as GPNodeHoldBegin;
                    if (d2.node == _model.Id)
                    {
                        _holdEffect.gameObject.SetActive(true);
                        _model.nodeViewManager.ShowPlusPoint(transform.localPosition, d2.point);
                    }

                    break;
                case EventName.GP_NODE_HOLD_UPDATE:
                    var d3 = data as GPNodeHoldUpdate;

                    if (d3.node == _model.Id)
                    {
                        _model.holdPercent = d3.holdPercent;
                        _holdEffect.value = _model.holdPercent;

                        if (d3.point > 0)
                        {
                            _model.nodeViewManager.ShowPlusPoint(transform.localPosition + new Vector3(0, _mainRect.sizeDelta.y), d3.point);
                        }
                    }
                    break;
                case EventName.GP_NODE_HOLD_END:
                    var d4 = data as GPNodeHoldEnd;
                    break;
                case EventName.GP_NODE_MISSING:
                    var data3 = data as GPNodeMissingEvent;
                    if (data3.nodeIds.Contains(_model.Id))
                    {
                        _missingEffect.gameObject.SetActive(true);
                        _missingEffect.DOFade(1, 0.3f)
                            .From(0)
                            .SetLoops(-1, LoopType.Yoyo);
                    }
                    break;
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            GamePlayService.Instance.DownNode(_model.Id, _model.nodeViewManager.GetDeltaPositionPercent(eventData.position.y - transform.position.y));
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            GamePlayService.Instance.UpNode(_model.Id);
        }
    }
}

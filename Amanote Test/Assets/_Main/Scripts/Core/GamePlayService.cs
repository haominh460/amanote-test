using DG.Tweening;
using Hawki;
using Hawki.Config.General;
using Hawki.EventObserver;
using Hawki.MyCoroutine;
using Hawki.Sound;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AmanotesTest.Core
{
    public enum GameStatus
    {
        Playing,
        Pausing,
        Ended,
    }

    public enum Compliment
    {
        None,
        Cool,
        Great,
        Perfect,
    }

    public class NodeType
    {
        public const string SIMPLE = "Simple";
        public const string LONG = "Long";
    }

    public class MapModel
    {
        public string id;
        public Dictionary<string, NodeModel> nodes = new Dictionary<string, NodeModel>();

        public float currentPosition;
        public float basePosition;

        public float multiSpeed = 1;
        public float baseSpeed;

        public GameStatus status;

        public List<string> lastVisibleNode = new List<string>();
        public List<string> nextCanInteractList = new List<string>();

        public Dictionary<string, float> holdingNodes = new Dictionary<string, float>();

        public int currentPoint;
        public float currentDeltaPositionPerfectPoint;

        public int perfectCombo;
        public float currentSpeed => baseSpeed * multiSpeed;

        public void PrepareNewStage()
        {
            foreach (var node in nodes.Values)
            {
                node.clicked = false;
                node.holdPercent = 0;
            }
        }
    }

    public class NodeModel
    {
        public string nodeId;
        public int columnId;
        public float length;
        public float beginPosition;

        public bool clicked;
        public float holdPercent;


        public string type;

        public bool InteractYet => !clicked && holdPercent == 0;
        public float EndPosition => beginPosition + length;
    }

    public class GamePlayService : RuntimeSingleton<GamePlayService>, IUpdateBehaviour
    {
        private MapModel _currentMap;

        private AudioSource _audioSources;

        public void InitBattle(string battleId)
        {
            _currentMap = new MapModel()
            {
                id = battleId,
                basePosition = -4100,
                baseSpeed = 1173,
            };
            _currentMap.currentPosition = _currentMap.basePosition;

            var faded = GeneralConfigManager.Instance.ConfigAll.Faded;

            _currentMap.nodes = faded.Select(x => new NodeModel
            {
                beginPosition = x.beginPosition * 800,
                columnId = x.columnId,
                length = x.length * 800,
                nodeId = Guid.NewGuid().ToString(),
                clicked = false,
                type = x.type,
            }).ToDictionary(x => x.nodeId);

            _currentMap.nodes = _currentMap.nodes.Values.OrderBy(x => x.beginPosition).ToDictionary(x => x.nodeId);

            UpdateNeedClickList();

            PlayAudio();

            EventObs.Instance.ExcuteEvent(EventName.GP_BEGIN, new GPBeginEvent
            {

            });
        }

        public void Update()
        {
            if (_currentMap == null || _currentMap.status != GameStatus.Playing)
            {
                return;
            }

            var deltaPosition = Time.deltaTime * _currentMap.currentSpeed;

            _currentMap.currentPosition += deltaPosition;

            UpdateHoldingNodes(deltaPosition);

            List<string> disableNodeList = UpdateVisible();

            var missingNodeList = disableNodeList.Where(x => _currentMap.nodes[x].InteractYet);

            if (missingNodeList.Any())
            {
                _currentMap.status = GameStatus.Ended;

                if (_audioSources != null)
                {
                    _audioSources.Stop();
                    _audioSources = null;
                }

                CoroutineManager.Instance.Start(MissingProgress(_currentMap.nextCanInteractList));
            };
        }

        private List<string> UpdateVisible()
        {
            var lastVisible = new List<string>(_currentMap.lastVisibleNode);
            var nextVisibleNode = new List<string>();

            var offsetPosition = Value.VISIBLE_POSITION_OFFSET;
            var minY = _currentMap.currentPosition - offsetPosition;
            var maxY = _currentMap.currentPosition + offsetPosition + Value.MAP_LENGTH;

            foreach (var nodeModel in _currentMap.nodes.Values)
            {
                if (nodeModel.EndPosition > minY && nodeModel.beginPosition < maxY)
                {
                    /*if (lastVisible.Contains(nodeModel.nodeId) == false)
                    {
                        Debug.LogError($"Add endPos = {nodeModel.EndPosition}, minY = {minY}, beginPos = {nodeModel.beginPosition}, maxY = {maxY}");
                    }*/
                    nextVisibleNode.Add(nodeModel.nodeId);
                }
            }
            _currentMap.lastVisibleNode = new List<string>(nextVisibleNode);

            var enableNodeList = nextVisibleNode.Except(lastVisible).ToList();
            var disableNodeList = lastVisible.Except(nextVisibleNode).ToList();
            var persistentNodeList = nextVisibleNode.Intersect(lastVisible).ToList();

            EventObs.Instance.ExcuteEvent(EventName.GP_NODE_DISABLE, new GPNodeDisableEvent()
            {
                nodeIds = new List<string>(disableNodeList),
            });

            EventObs.Instance.ExcuteEvent(EventName.GP_NODE_ENABLE, new GPNodeEnableEvent()
            {
                nodeIds = new List<string>(enableNodeList),
                dictBeginPositionPercent = enableNodeList.ToDictionary(x => x, x => CurrentPercent(x)),
                dictColumn = enableNodeList.ToDictionary(x => x, x => _currentMap.nodes[x].columnId),
                dictPercentLength = enableNodeList.ToDictionary(x => x, x => LengthPercent(_currentMap.nodes[x].length)),
                dictType = enableNodeList.ToDictionary(x => x, x => _currentMap.nodes[x].type),
                dictClicked = enableNodeList.ToDictionary(x => x, x => _currentMap.nodes[x].clicked),
                dictHoldPercent = enableNodeList.ToDictionary(x => x, x => _currentMap.nodes[x].holdPercent),
            });

            EventObs.Instance.ExcuteEvent(EventName.GP_NODE_UPDATE, new GPNodeUpdateEvent()
            {
                _dictPercentPosition = persistentNodeList.ToDictionary(x => x, x => CurrentPercent(x))
            });

            return disableNodeList;
        }

        private void UpdateHoldingNodes(float deltaPosition)
        {
            var fullNodes = new List<string>();

            foreach (var node in _currentMap.holdingNodes)
            {
                var nodeModel = _currentMap.nodes[node.Key];

                nodeModel.holdPercent += (deltaPosition / nodeModel.length);

                int point = 0;
                if (nodeModel.holdPercent >= 1)
                {
                    nodeModel.holdPercent = 1;

                    fullNodes.Add(node.Key);

                    point = 2;
                    AddPoint(node.Key, point, Compliment.None);
                }

                EventObs.Instance.ExcuteEvent(EventName.GP_NODE_HOLD_UPDATE, new GPNodeHoldUpdate
                {
                    node = node.Key,
                    holdPercent = nodeModel.holdPercent,
                    point = point,
                    totalPoint = _currentMap.currentPoint,
                });
            }

            fullNodes.ForEach(x => _currentMap.holdingNodes.Remove(x));
        }

        IEnumerator MissingProgress(List<string> nodeIds)
        {
            var nodeModel = _currentMap.nodes[nodeIds.First()];

            var nextPosition = nodeModel.beginPosition;
            var oldPosition = _currentMap.currentPosition;
            DOVirtual.Float(oldPosition, nextPosition, 0.3f, (x) =>
            {
                _currentMap.currentPosition = x;
                UpdateVisible();
            });

            yield return new WaitForSeconds(0.3f);

            EventObs.Instance.ExcuteEvent(EventName.GP_NODE_MISSING, new GPNodeMissingEvent()
            {
                nodeIds = nodeIds
            });

            CoroutineManager.Instance.Start(ReplayStage());
        }

        public float CalculateTime(float length)
        {
            return length / _currentMap.currentSpeed;
        }

        public void ClickNode(string nodeId)
        {
            if (_currentMap == null || _currentMap.status != GameStatus.Playing)
            {
                return;
            }

            var nodeModel = _currentMap.nodes[nodeId];
            if (nodeModel.clicked)
            {
                return;
            }

            if (_currentMap.nextCanInteractList.Contains(nodeId) == false)
            {
                // return or Lose ???
                return;
            }

            nodeModel.clicked = true;

            CalculatePoint(nodeId, out int point);

            _currentMap.currentDeltaPositionPerfectPoint = nodeModel.beginPosition - _currentMap.currentPosition;

            _currentMap.nextCanInteractList.Remove(nodeId);

            if (_currentMap.nextCanInteractList.Count == 0)
            {
                UpdateNeedClickList();
            }


            EventObs.Instance.ExcuteEvent(EventName.GP_NODE_CLICK, new GPNodeClickEvent()
            {
                node = nodeId,
                point = point,
                totalPoint = _currentMap.currentPoint,
            });
        }

        private void CalculatePoint(string nodeId, out int point)
        {
            if (_currentMap == null)
            {
                point = 0;
                return;
            }

            var nodeModel = _currentMap.nodes[nodeId];

            var perfectPosition = _currentMap.currentDeltaPositionPerfectPoint + _currentMap.currentPosition;
            var absPosition = Mathf.Abs(nodeModel.beginPosition - perfectPosition);

            if (_currentMap.currentDeltaPositionPerfectPoint == 0 || absPosition < 100)
            {
                _currentMap.perfectCombo++;
                point = 3;
                AddPoint(nodeId, 3, Compliment.Perfect);
            } else if (absPosition < 200)
            {
                _currentMap.perfectCombo = 0;
                point = 2;
                AddPoint(nodeId, 2, Compliment.Great);
            } else
            {
                _currentMap.perfectCombo = 0;
                point = 1;
                AddPoint(nodeId, 1, Compliment.Cool);
            }
        }

        private void AddPoint(string nodeId, int point, Compliment compliment = Compliment.None)
        {
            _currentMap.currentPoint += point;

            EventObs.Instance.ExcuteEvent(EventName.GP_POINT_ADD, new GPPointAddEvent
            {
                nodeId = nodeId,
                pointAdd = point,
                perfectCombo = _currentMap.perfectCombo,
                totalPoint = _currentMap.currentPoint,
                compliment = compliment,
            });
        }

        public void DownNode(string nodeId, float deltaPercentPosition)
        {
            if (_currentMap == null || _currentMap.status != GameStatus.Playing)
            {
                return;
            }

            if (_currentMap.holdingNodes.ContainsKey(nodeId))
            {
                return;
            }

            var nodeModel = _currentMap.nodes[nodeId];
            if (nodeModel.holdPercent > 0)
            {
                return;
            }

            if (_currentMap.nextCanInteractList.Contains(nodeId) == false)
            {
                return;
            }

            var deltaPosition = deltaPercentPosition * Value.MAP_LENGTH;
            nodeModel.holdPercent = deltaPosition / nodeModel.length;
            _currentMap.holdingNodes.Add(nodeId, _currentMap.currentPosition);

            CalculatePoint(nodeId, out int point);
            _currentMap.currentDeltaPositionPerfectPoint = nodeModel.beginPosition - _currentMap.currentPosition;

            _currentMap.nextCanInteractList.Remove(nodeId);
            if (_currentMap.nextCanInteractList.Count == 0)
            {
                UpdateNeedClickList();
            }

            EventObs.Instance.ExcuteEvent(EventName.GP_NODE_HOLD_BEGIN, new GPNodeHoldBegin
            {
                node = nodeId,
                point = point,
                totalPoint = _currentMap.currentPoint,
            });
        }

        public void UpNode(string nodeId)
        {
            if (_currentMap == null)
            {
                return;
            }

            if (_currentMap.holdingNodes.ContainsKey(nodeId) == false)
            {
                return;
            }

            _currentMap.holdingNodes.Remove(nodeId);

            EventObs.Instance.ExcuteEvent(EventName.GP_NODE_HOLD_END, new GPNodeHoldEnd
            {
                node = nodeId
            });
        }

        public void ColumnDown(int id)
        {
            if (_currentMap == null || _currentMap.status != GameStatus.Playing)
            {
                return;
            }

            _currentMap.status = GameStatus.Ended;

            EventObs.Instance.ExcuteEvent(EventName.GP_COLUMN_DOWN, new GPColumnClickEvent
            {
                columnId = id,
            });

            if (_audioSources != null)
            {
                _audioSources.Stop();
                _audioSources = null;
            }

            CoroutineManager.Instance.Start(ReplayStage());
        }

        private void UpdateNeedClickList()
        {
            _currentMap.nextCanInteractList = new List<string>();

            var findAll = _currentMap.nodes.Values.Where(x => x.InteractYet);
            if (findAll.Any())
            {
                var beginPosition = findAll.Min(x => x.beginPosition);
                _currentMap.nextCanInteractList = _currentMap.nodes.Values.Where(x => x.beginPosition == beginPosition).Select(x => x.nodeId).ToList();
            } else
            {
                CoroutineManager.Instance.Start(NextStage(0.2f));
            }
        }

        public float CurrentPercent(string nodeId)
        {
            if (_currentMap == null)
            {
                return 0;
            }

            var minY = _currentMap.currentPosition;
            var maxY = _currentMap.currentPosition + Value.MAP_LENGTH;

            var position = _currentMap.nodes[nodeId].beginPosition;

            if (maxY - minY == 0)
            {
                return 0f;
            }

            return (position - maxY) / (minY - maxY);
        }

        public float LengthPercent(float length)
        {
            return length / Value.MAP_LENGTH;
        }

        private IEnumerator NextStage(float modifySpeed)
        {
            yield return new WaitForSeconds(4 / _currentMap.multiSpeed);

            _currentMap.PrepareNewStage();
            _currentMap.currentPosition = _currentMap.basePosition;
            _currentMap.multiSpeed += modifySpeed;
            _currentMap.currentDeltaPositionPerfectPoint = 0;

            UpdateNeedClickList();

            PlayAudio();
        }

        private IEnumerator ReplayStage()
        {
            yield return new WaitForSeconds(2);

            InitBattle(string.Empty);
        }

        private void PlayAudio()
        {
            if (_audioSources != null)
            {
                _audioSources.Stop();
            }

            _audioSources = SoundManager.Instance.PlaySound("Faded");
            _audioSources.pitch = _currentMap.multiSpeed;
        }
    }
}
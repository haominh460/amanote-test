using Hawki.EventObserver;
using System.Collections.Generic;

namespace Hawki.EventObserver
{
    public partial class EventName
    {

        public const string GP_BEGIN = "GP_BEGIN";
        public const string GP_NODE_ENABLE = "GP_NODE_ENABLE";
        public const string GP_NODE_UPDATE = "GP_NODE_UPDATE";
        public const string GP_NODE_DISABLE = "GP_NODE_DISABLE";
        public const string GP_NODE_CLICK = "GP_NODE_CLICK";
        public const string GP_NODE_HOLD_BEGIN = "GP_NODE_HOLD_BEGIN";
        public const string GP_NODE_HOLD_UPDATE = "GP_NODE_HOLD_UPDATE";
        public const string GP_NODE_HOLD_END = "GP_NODE_HOLD_END";
        public const string GP_NODE_MISSING = "GP_NODE_MISSING";
        public const string GP_POINT_ADD = "GP_POINT_ADD";
        public const string GP_COLUMN_DOWN = "GP_COLUMN_DOWN";
        public const string GP_REPLAY = "GP_REPLAY";
    }
}

namespace AmanotesTest.Core
{
    public class GPBeginEvent : EventBase
    {

    }

    public class GPNodeEnableEvent : EventBase
    {
        public List<string> nodeIds = new List<string>();
        public Dictionary<string, float> dictBeginPositionPercent = new Dictionary<string, float>();
        public Dictionary<string, int> dictColumn = new Dictionary<string, int>();
        public Dictionary<string, float> dictPercentLength = new Dictionary<string, float>();
        public Dictionary<string, string> dictType = new Dictionary<string, string>();
        public Dictionary<string, bool> dictClicked = new Dictionary<string, bool>();
        public Dictionary<string, float> dictHoldPercent = new Dictionary<string, float>();
    }
    public class GPNodeUpdateEvent : EventBase
    {
        public Dictionary<string, float> _dictPercentPosition = new Dictionary<string, float>();
    }

    public class GPNodeDisableEvent : EventBase
    {
        public List<string> nodeIds = new List<string>();
    }

    public class GPNodeClickEvent : EventBase
    {
        public string node;
        public int point;
        public int totalPoint;
    }

    public class GPNodeHoldBegin : EventBase
    {
        public string node;
        public int point;
        public int totalPoint;
    }

    public class GPNodeHoldUpdate : EventBase
    {
        public string node;
        public float holdPercent;
        public int point;
        public int totalPoint;
    }

    public class GPNodeHoldEnd : EventBase
    {
        public string node;
        public int point;
        public int totalPoint;
    }

    public class GPNodeMissingEvent : EventBase
    {
        public List<string> nodeIds;
    }

    public class GPPointAddEvent : EventBase
    {
        public string nodeId;
        public Compliment compliment;
        public int perfectCombo;
        public int pointAdd;
        public int totalPoint;
    }

    public class GPColumnClickEvent : EventBase
    {
        public int columnId;
    }

    public class GPReplayEvent : EventBase
    {

    }
}

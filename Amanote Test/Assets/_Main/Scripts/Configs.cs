using AmanotesTest;
using System.Collections.Generic;

namespace Hawki.AllConfig
{
    public partial class ConfigAll
    {
        public List<NodeConfig> Faded = new List<NodeConfig>();
    }
}

namespace AmanotesTest
{
    public class NodeConfig
    {
        public int columnId;
        public float beginPosition;
        public float length;
        public string type;
    }
}
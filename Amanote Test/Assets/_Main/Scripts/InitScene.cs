using Hawki.Scene;
using Hawki.Sound;
using UnityEngine;

namespace AmanotesTest
{
    public class InitScene : MonoBehaviour
    {
        private void Awake()
        {
            Application.targetFrameRate = 60;
            Input.multiTouchEnabled = true;

            SceneManager.Instance.SetSceneLoader(new PrefabLoader("Prefabs/Scenes", new GameObject("[Scene] Root").transform));
        }

        private void Start()
        {
            SoundManager.Instance.sound = 1;

            SceneManager.Instance.OpenScene(SceneId.GAMEPLAY);
        }
    }
}

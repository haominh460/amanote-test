// This file is automatically generated. Do not modify!
// Only operates when the compiler has no errors.

using Hawki.Localization;
using Hawki.AllConfig;

namespace Hawki.Config.General
{
    public class GeneralConfigManager : RuntimeSingleton<GeneralConfigManager>
    {
        public LocalizationConfig LocalizationConfig => ConfigManager.Instance.GetData<LocalizationConfig>();
        public ConfigAll ConfigAll => ConfigManager.Instance.GetData<ConfigAll>();
    }
}
